var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
    user_name: String,
    first_name: String,
    last_name: String
});

module.exports = mongoose.model('User', UserSchema);