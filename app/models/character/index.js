var mongoose = require('mongoose')
var Schema = mongoose.Schema

var Character = new Schema({
  name: String,
  eyeColor: String,
  height: String,
  weight: Number,
  skinColor: String,
  hairColor: String,
  age: Number,
  background: String,
  ideals: String,
  alignment: String,
  bonds: String,
  flaws: String,
  mainHand: String,
  offHand: String,
  equipment: [],
  treasure: [],
  gold: Number,
  strength: Number,
  dexterity: Number,
  constitution: Number,
  intelligence: Number,
  wisdom: Number,
  charisma: Number,
  race: String,
  charClass: String,
  armorClass: String,
  speed: Number,
  initiative: String,
  health: Number,
  level: Number,
  languages: [],
  spells: [],
  traits: []
})

module.exports = mongoose.model('Character', Character)
