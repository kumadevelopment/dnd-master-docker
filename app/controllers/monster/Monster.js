
Monster = require('../../models/monster')
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

exports.createMonster = (req, res, err) => {
  console.log(req)
  const accountId = req.decoded.id
  var email = req.body.email
  var strength = req.body.strength
  var dexterity = req.body.dexterity
  var constitution = req.body.constitution
  var intelligence = req.body.intelligence
  var wisdom = req.body.wisdom
  var charisma = req.body.charisma
  var name = req.body.name
  var rank = req.body.rank
  var armorClass = req.body.armorClass
  var speed = req.body.speed
  var initiative = req.body.initiative
  var health = req.body.health
  var level = req.body.level
  var languages = req.body.languages
  var spells = req.body.spells
  var type = req.body.type
  var alignment = req.body.alignment
  var acrobatics = req.body.acrobatics
  var animalHandling = req.body.animalHandling
  var arcana = req.body.arcana
  var athletics = req.body.athletics
  var deception = req.body.deception
  var history = req.body.history
  var insight = req.body.insight
  var intimidation = req.body.intimidation
  var investigation = req.body.investigation
  var medicine = req.body.medicine
  var nature = req.body.nature
  var perception = req.body.perception
  var performance = req.body.performance
  var persuasion = req.body.persuasion
  var religion = req.body.religion
  var slightOfhand = req.body.slightOfhand
  var stealth = req.body.stealth
  var survival = req.body.survival
  var mainHand = req.body.mainHand
  var offHand = req.body.offHand
  var equipment = req.body.equipment
  var treasure = req.body.treasure
  var gold = req.body.gold

  var monster = new Monster({
    account: accountId,
    strength: strength,
    dexterity: dexterity,
    constitution: constitution,
    intelligence: intelligence,
    wisdom: wisdom,
    charisma: charisma,
    name: name,
    rank: rank,
    armorClass: armorClass,
    speed: speed,
    initiative: initiative,
    health: health,
    level: level,
    languages: languages,
    spells: spells,
    type: type,
    alignment: alignment,
    acrobatics: acrobatics,
    animalHandling: animalHandling,
    arcana: arcana,
    athletics: athletics,
    deception: deception,
    history: history,
    insight: insight,
    intimidation: intimidation,
    investigation: investigation,
    medicine: medicine,
    nature: nature,
    perception: perception,
    performance: performance,
    persuasion: persuasion,
    religion: religion,
    slightOfhand: slightOfhand,
    stealth: stealth,
    survival: survival,
    mainHand: mainHand,
    offHand: offHand,
    equipment: equipment,
    treasure: treasure,
    gold: gold,
  })

  monster.save((err) => {
    if (err) {
      res.status(err.statusCode || 500).json(err);
    } else {
      res.status(200).json({
        messgage: 'Monster created'
      })
    }
  })
}