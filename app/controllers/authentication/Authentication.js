const passport = require('passport'),  
      Account = require('../../models/account')
      jwt = require('jsonwebtoken')

// Register
exports.register = (req, res, err) => {
  
  var email = req.body.email
  var password = req.body.password
  var account = new Account({
    email: email,
    password: password
  })
  

  this.exists = function(email) {
    Account.findOne({ email: email }, function(err, data) {
      if (err) {
        res.status(err.statusCode || 500).json(err);
      }
    })
  }
  this.exists(email);
  account.save((err) => {
    if (err) {
      res.status(err.statusCode || 500).json(err);
    } else {
      res.status(200).json({
        messgage: 'Account created'
      })
    }
  })
}

exports.login = (req, res, err) => {
  console.log(req.body)
  Account.findOne({email: req.body.email}, function(err, account) {
    if (err) {
      res.status(500).json(err)
    } else {
      account.comparePassword(req.body.password, function(err, isMatch) {
        if (isMatch && !err) {
          const payload = {
            id: account._id
          }
          var token = jwt.sign(payload, 'dndMaster', {
            expiresIn: '1d'
          });

          res.json({
            success: true,
            token: token
          })
        } else {
          res.status(500).json('incorrect password');
        }
      });
    }
  })
}
