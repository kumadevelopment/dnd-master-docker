const express = require('express')
const Authentication = require('../controllers/authentication/Authentication')
const Character = require('../controllers/character/Character')
const Monster = require('../controllers/monster/Monster')
const jwt = require('jsonwebtoken')

module.exports = (app) => {
  var apiRoutes = express.Router()
  var authRoutes = express.Router()
  var characterRoutes = express.Router()
  var monsterRoutes = express.Router()

  // Auth Routes
  authRoutes.post('/register', Authentication.register)
  authRoutes.post('/authenticate', Authentication.login)

  apiRoutes.use('/auth', authRoutes)

  apiRoutes.use( function(req, res, next) {
    console.log(req.headers)
    var token = req.headers['authorization']
    console.log(token);
    if (token) {
      console.log(token)
      jwt.verify(token, 'dndMaster', function(err, decoded) {
        if (err) {
          return res.json({
            success: false,
            message: 'Failed to authenticate token'
          })
        } else {
          req.decoded = decoded
          next()
        }
      })
    } else {
      return res.json({
        error: 'no token provided'
      })
    }
  })

  // route group set
  apiRoutes.use('/characters', characterRoutes)
  apiRoutes.use('/monsters', monsterRoutes)

  // Character Routes
  characterRoutes.get('/:accountId', Character.characters)
  

  //Monster Routes
  monsterRoutes.post('/create', Monster.createMonster)

  // API Main route
  app.use('/api', apiRoutes)
}
