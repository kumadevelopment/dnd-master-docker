FROM node:9.3.0

RUN mkdir /src

WORKDIR /src
ADD app/package.json /src/package.json
RUN npm install

RUN npm install nodemon -g
RUN apt-get -qq update
RUN apt-get -qq -y install curl
RUN apt-get install bcrypt

ADD app/nodemon.json /src/nodemon.json

EXPOSE 3000

CMD npm start